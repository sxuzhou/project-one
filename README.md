# Enterprise Reimbursement Systems

## Project Descrition
Enterprise Reimbursement Systems (ERS) is an application that allows employees to register accounts and create reimbursements.
It also allows for administrators to approve or deny applications and change the status of reimbursements.

ERS is build on pure java leveraging servlet technology to communicate with HTML/JS pages.


## Technologies Used
- Java
- Junit
- Jackson
- Javax
- PosgreSQL
- JDBC


## Getting Started

1. git clone https://gitlab.com/sxuzhou/project-one.git
1. configure the DbConnection file so that it points to the correct database
1. Run application as a tomcat WAR file.

## Usage

A user should register an account and login. Then create a reimbursements, filling out information such as the type of reimbursements, the amount, and a description.Then have an administrator account approve or deny the reimbursement.
