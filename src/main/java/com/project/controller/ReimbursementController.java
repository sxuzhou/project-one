package com.project.controller;

import java.sql.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.project.dao.ReimbursementDao;
import com.project.model.Reimbursement;
import com.project.service.ReimbursementService;
import com.project.servlet.MasterServlet;

public class ReimbursementController {
	static ReimbursementService reburServ = new ReimbursementService(new ReimbursementDao());
	
	
	public static String createReimbursement(HttpServletRequest req) {
		if(!req.getMethod().equals("POST")) {
			return "html/createReimbursement.html";
		}
		String u = "";
		HttpSession session = req.getSession(false);
		if(session != null) {

			u =  (String) session.getAttribute("username");
			
		}
		Reimbursement rebur = new Reimbursement(Double.valueOf(req.getParameter("amount")),
				req.getParameter("date"),
				req.getParameter("description"), 2 ,Integer.valueOf( req.getParameter("catagory")));
		reburServ.createReimbursement(rebur, u);
		return "html/backToTable.html";
		
		
		
	}
}
