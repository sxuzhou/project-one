package com.project.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.project.dao.UserDao;
import com.project.model.User;
import com.project.service.UserService;

public class UserController {
	static UserService userServ = new UserService(new UserDao());
	
	public static String login(HttpServletRequest req) {
		String username = req.getParameter("username");
		User user = userServ.login(username, req.getParameter("password"));
		if ( user != null) {
			HttpSession session = req.getSession();
			session.setAttribute("username", user.getUsername());
			session.setAttribute("role", user.getRole());
			if(user.getRole().equals("Employee")) {

				return "/html/employeeMenu.html";
			} else {
				return "/html/managerMenu.html";
			}
		} else {
			return "/html/login.html";
		}
	}
	public static String logout(HttpServletRequest req) {
		HttpSession session = req.getSession();
		session.invalidate();
		return "/html/logoutSucess.html";
	}
	

}
