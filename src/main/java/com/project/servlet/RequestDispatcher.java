package com.project.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.project.controller.ReimbursementController;
import com.project.controller.UserController;

public class RequestDispatcher {

	public static String process(HttpServletRequest req) {

		HttpSession session = req.getSession();
		switch (req.getRequestURI()) {
		case "/ProjectOne/Chart.change":
			if (session == null || session.getAttribute("username") == null) {
		        return "html/login.html";
		    }
			return "html/tableView.html";
			
		case "/ProjectOne/Create.change":
			 if (session == null || session.getAttribute("username") == null) {
			        return "html/login.html";
			    }
			return ReimbursementController.createReimbursement(req);
		
		case "/ProjectOne/ApprovalChart.change":
			if (session == null || session.getAttribute("username") == null) {
		        return "html/login.html";
		    }
			return "html/approvalTable.html";
			
		case "/ProjectOne/ApproveOrDenyChart.change":
			if (session == null || session.getAttribute("username") == null) {
		        return "html/login.html";
		    }
			return "html/approveOrDenyTable.html";
			
		case "/ProjectOne/Login.change": {
			return UserController.login(req);
			
		}
		case"/ProjectOne/Logout.change":{
			return UserController.logout(req);
		}

		default:
			return "html/index.html";

		}

	}
}
