package com.project.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.dao.ReimbursementDao;
import com.project.model.Reimbursement;

public class JSONDispatcherServlet extends HttpServlet {

	public static void process(HttpServletRequest req, HttpServletResponse res)
			throws JsonProcessingException, IOException {
		
		
		
		if (req.getRequestURI().equals("/ProjectOne/getChart.json")) {
			List<Reimbursement> reburList = new ArrayList<>();
			ReimbursementDao reburDao = new ReimbursementDao();
			try
			{
				HttpSession session = req.getSession(false);
				if(session != null) {

					String u =  (String) session.getAttribute("username");
					reburList = reburDao.getAllFromUser(u);
				}
				} catch (SQLException e) {
					MasterServlet.log.error(e);
				}
			res.getWriter().write(new ObjectMapper().writeValueAsString(reburList));
			}
		else if (req.getRequestURI().equals("/ProjectOne/getAllChart.json"))
		{	
			HttpSession session = req.getSession(false);
			List<Reimbursement> reburList2 = new ArrayList<>();
			if(session != null) {

				String u =  (String) session.getAttribute("role");
				if (u.equals("Finance Manager")) {
					

					ReimbursementDao reburDao2 = new ReimbursementDao();
					try {
						reburList2 = reburDao2.getAll();
					} catch (SQLException e) {

						MasterServlet.log.error(e);
					}
				}
			}
			
			res.getWriter().write(new ObjectMapper().writeValueAsString(reburList2));
			
		}
		else if(req.getRequestURI().equals("/ProjectOne/updateStatus.json")) {
			ReimbursementDao reburDao = new ReimbursementDao();
			int id = Integer.valueOf(req.getParameter("id"));
			String type = req.getParameter("type");
			boolean isApproved;
			if(type.equals("approve")){
				isApproved = true;
			} else {
				isApproved = false;
			}
			try {
				reburDao.updateStatus(id, isApproved);
			} catch (SQLException e) {

				MasterServlet.log.error(e);
			}
			res.getWriter().write(new ObjectMapper().writeValueAsString("sucess"));

			
		}
	}

}
