package com.project.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.project.servlet.MasterServlet;

public class DbConnection {

	private final String URL = "jdbc:postgresql://rev-can-trainning.cqrdgjlr1vfb.us-east-2.rds.amazonaws.com:5432/ersdb";
	private final String USERNAME = "ersuser";
	private final String PASSWORD = "Passw0rd";

	public Connection getDbConnection() throws SQLException {
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			MasterServlet.log.error(e.toString());
		}
		return DriverManager.getConnection(URL, USERNAME, PASSWORD);
	}

}
