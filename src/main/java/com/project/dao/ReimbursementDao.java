package com.project.dao;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.project.model.Reimbursement;
import com.project.model.User;

public class ReimbursementDao {
	
	private DbConnection dbCon;
	
	public ReimbursementDao() {
		dbCon = new DbConnection();
	}
	
	public List<Reimbursement> getAllFromUser(String u) throws SQLException{
		List<Reimbursement> reburList = new ArrayList<>();
		Reimbursement rebur;
		Connection con = dbCon.getDbConnection();
		String sql = "select ujr.user_name, r.id, r.amount , r.resolved, r.description ,rs.id, rt.id from user_junction_reimbursement ujr left outer join reimbursement r  on ujr.reimbursement_id = r.id"
				+ " left outer join reimbursement_junction_reimbursement_status rjrs on r.id = rjrs.reimbursement_id"
				+ " left outer join reimbursement_status rs on rjrs.reimbursement_status_id = rs.id"
				+ " left outer join reimbursement_junction_reimbursement_type rjrt on r.id = rjrt.reimbursement_id"
				+ " left outer join reimbursement_type rt on rjrt.reimbursement_type_id = rt.id  where ujr.user_name = ?";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, u);
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			rebur = new Reimbursement(rs.getInt(2), rs.getDouble(3), rs.getString(4), rs.getString(5), rs.getInt(6), rs.getInt(7));
			reburList.add(rebur);
		}
		return reburList;
	}
	public List<Reimbursement> getAll() throws SQLException{
		List<Reimbursement> reburList = new ArrayList<>();
		Reimbursement rebur;
		Connection con = dbCon.getDbConnection();
		String sql = "select ujr.user_name, r.id, r.amount , r.resolved, r.description ,rs.id, rt.id from user_junction_reimbursement ujr left outer join reimbursement r  on ujr.reimbursement_id = r.id"
				+ " left outer join reimbursement_junction_reimbursement_status rjrs on r.id = rjrs.reimbursement_id"
				+ " left outer join reimbursement_status rs on rjrs.reimbursement_status_id = rs.id"
				+ " left outer join reimbursement_junction_reimbursement_type rjrt on r.id = rjrt.reimbursement_id"
				+ " left outer join reimbursement_type rt on rjrt.reimbursement_type_id = rt.id";
		PreparedStatement ps = con.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			rebur = new Reimbursement(rs.getInt(2), rs.getDouble(3), rs.getString(4), rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getString(1));
			reburList.add(rebur);
		}
		return reburList;
	}
	
	public void insert(Reimbursement rebur, String username) throws SQLException {
		Connection con = dbCon.getDbConnection();
		String sql = "{? = call insert_reimbursement(?,?,?,?,?,?)}";
		CallableStatement cs = con.prepareCall(sql);
		cs.registerOutParameter(1, Types.VARCHAR);
		cs.setBigDecimal(2, BigDecimal.valueOf(rebur.getAmount()));
		cs.setString(3, rebur.getResolved());
		cs.setString(4, rebur.getDescription());
		cs.setInt(5, rebur.getStatus());
		cs.setInt(6, rebur.getCatagory());
		cs.setString(7, username);
		cs.execute();
		
	}
	
	public void updateStatus(int id, boolean isApproved) throws SQLException {
		Connection con = dbCon.getDbConnection();
		String sql = "update reimbursement_junction_reimbursement_status set reimbursement_status_id = ? where reimbursement_id = ?";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setInt(1, isApproved ? 1: 3);
		ps.setInt(2, id);
		ps.execute();
	}
	

}
