package com.project.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.project.model.User;

public class UserDao {
		private DbConnection dbCon;
		
		public UserDao() {
			dbCon = new DbConnection();
		}
		
		public List<User> getAll() throws SQLException{
			List<User> userList = new ArrayList<>();
			Connection con = dbCon.getDbConnection();

			String sql = "select * from users u left outer join user_junction_user_roles ujur on u.username = ujur.user_name left outer join user_roles ur on ujur.type_id = ur.id";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				userList.add(new User(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(9)));
			}
			return userList;
		}
		
		public User getByName(String username) throws SQLException {

			User user = new User();
			try (Connection con = dbCon.getDbConnection()) {
				String sql = "select * from users u left outer join user_junction_user_roles ujur on u.username = ujur.user_name left outer join user_roles ur on ujur.type_id = ur.id where u.username = ?";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, username);
				ResultSet rs = ps.executeQuery();

				while (rs.next()) {
					user = new User(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(9));
				}

			} catch (SQLException e) {
				throw e;
			}
			return user;
		}
		
}
