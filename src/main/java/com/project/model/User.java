package com.project.model;

import lombok.Data;

@Data
public class User {
	
	private String username;
	private String password;
	private String firstname;
	private String lastname;
	private String email;
	private String role;
	
	public User(String username, String password, String firstname, String lastname, String email, String role) {
		super();
		this.username = username;
		this.password = password;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.role = role;
	}

	public User() {
		// TODO Auto-generated constructor stub
	}
	
	

}
