package com.project.model;
import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class Reimbursement {
	
	

	private int id;
	private double amount;
	private String resolved;
	private String description;
	private int status;
	private int catagory;
	private String username;

	public String getStatusString() {
		if (this.status == 1) {
			return "approved";
		} else if (this.status == 2) {
			return "pending";
		} else {
			return "denied";
		}

	}

	public String getCatagoryString() {
		if (this.catagory == 1) {
			return "food";
		} else if (this.catagory == 2) {
			return "travel";
		} else {
			return "supplies";
		}

	}

	public Reimbursement(double amount, String resolved, String description, int status, int catagory) {
		super();
		this.amount = amount;
		this.resolved = resolved;
		this.description = description;
		this.status = status;
		this.catagory = catagory;
	}
	
	public Reimbursement(int id, double amount, String resolved, String description, int status, int catagory) {
		super();
		this.id = id;
		this.amount = amount;
		this.resolved = resolved;
		this.description = description;
		this.status = status;
		this.catagory = catagory;
	}

	public Reimbursement(int id, double amount, String resolved, String description, int status, int catagory,
			String username) {
		super();
		this.id = id;
		this.amount = amount;
		this.resolved = resolved;
		this.description = description;
		this.status = status;
		this.catagory = catagory;
		this.username = username;
	}
	


}
