package com.project.service;

import java.sql.SQLException;

import com.project.dao.ReimbursementDao;
import com.project.model.Reimbursement;
import com.project.servlet.MasterServlet;

public class ReimbursementService {
	private ReimbursementDao reburDao;
	
	public ReimbursementService( ReimbursementDao reburDao) {
		this.reburDao = reburDao;
	}
	
	public void createReimbursement(Reimbursement rebur, String username) {
		try {
			reburDao.insert(rebur, username);
		} catch (SQLException e) {
			MasterServlet.log.error(e.toString());
		}
	}
}
