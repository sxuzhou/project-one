package com.project.service;

import java.sql.SQLException;

import com.project.dao.UserDao;
import com.project.model.User;
import com.project.servlet.MasterServlet;

public class UserService {
	private UserDao userDao;
	
	public UserService(UserDao userDao) {
		this.userDao = userDao;
	}
	
	public User login(String username, String password) {
		try {
			User u = userDao.getByName(username);
			if(u.getUsername() == null) {
				return null;
			}
			if(u.getPassword().equals(password)) {
				return u;
			} else {
				return null;
			}
			
		} catch (SQLException e) {
			MasterServlet.log.error(e);
			return null;
		}
		
	}

}
