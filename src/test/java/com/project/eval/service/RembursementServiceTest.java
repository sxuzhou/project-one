package com.project.eval.service;

import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;


import java.sql.SQLException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;



import com.project.dao.ReimbursementDao;
import com.project.model.Reimbursement;
import com.project.service.ReimbursementService;


public class RembursementServiceTest {
	
	@Mock
	private ReimbursementDao fakeDao;
	private ReimbursementService reburServ;
	private Reimbursement rebur;
	
	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);
		reburServ = new ReimbursementService(fakeDao);
		rebur = new Reimbursement(1, 400, "1990-12-25", "test ticket", 1, 1, "Sam");
		
		try {
			doNothing().when(fakeDao).insert(rebur, rebur.getUsername());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void insertSucess() {
		assertDoesNotThrow(() -> reburServ.createReimbursement(rebur, rebur.getUsername()));
	}

}
