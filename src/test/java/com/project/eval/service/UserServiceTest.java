package com.project.eval.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.sql.SQLException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.project.dao.UserDao;
import com.project.model.User;
import com.project.service.UserService;

public class UserServiceTest {
	
	@Mock
	private UserDao userDao;
	private UserService userServ;
	private User user;
	
	@BeforeEach
	public void setUp(){
		
		MockitoAnnotations.initMocks(this);
		userServ = new UserService(userDao);
		user = new User("Sam", "Sam", "Sam", "Sam", "Sam", "Sam");
		try {
			when(userDao.getByName("Sam")).thenReturn(user);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void loginSucess() {
		assertEquals(user, userServ.login(user.getUsername(),user.getPassword()));
	}
	@Test
	public void loginFails() {
		assertEquals(null, userServ.login(user.getUsername(),"not correct password"));
	}
	

}
